console.log("whatdahel is barek?!")

// mock database
let posts = [];

// post ID
let count = 1;

// add post
document.querySelector("#form-add-post").addEventListener("submit", (event => {

	//Prevents the page from reloading
	event.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	})
    // Iincrement everytime new post
	count++;
	alert("successfully added!")
	showPost(posts)
}))


// show post
const showPost = (posts) => {

    // variable that will contain all the post
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `
		<div id="post-${post.id}">
		<h3 id="post-title-${post.id}">${post.title}</h3>
		<p id="post-body-${post.id}">${post.body}</p>
		<button onclick="editPost('${post.id}')">Edit</button>
		<button onclick="deletePost('${post.id}')">Delete</button>

		</div> `
	});

    // display post
	document.querySelector("#div-post-entries").innerHTML = postEntries
}


//edit post
//this will trigger an event that will update a certain post upon clicking the edit button.

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector(`#txt-edit-id`).value = id;
	document.querySelector(`#txt-edit-title`).value = title;
	document.querySelector(`#txt-edit-body`).value = body;
}

document.querySelector('#form-edit-post').addEventListener("submit", (event) => {
	event.preventDefault();

	for (let i = 0; i < posts.length; i++) {
		if (posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {
			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value;

			showPost(posts);
			alert("Successfully updated");

			break;
		}
	}
});

//alternative
/*const form = document.querySelector('#form-edit-post');
form.addEventListener("submit", (event) => {
  event.preventDefault();

  const editId = document.querySelector("#txt-edit-id").value;
  const post = posts.find(p => p.id.toString() === editId);
  
  if (post) {
    post.title = document.querySelector("#txt-edit-title").value;
    post.body = document.querySelector("#txt-edit-body").value;

    showPost(posts);
    alert("Successfully updated");
  }
});
*/

//delete post
const deletePost = (id) => {
    for (let i = 0; i < posts.length; i++) {
        if (posts[i].id.toString() === id) {
            posts.splice(i, 1);
            showPost(posts);
            alert("Successfully deleted");
            break;
        }
    }
};

//alternative code
/*const deletePost = (id) => {
    const postIndex = posts.findIndex(p => p.id.toString() === id);
    if (postIndex >= 0) {
        posts.splice(postIndex, 1);
        showPost(posts);
        alert("Successfully deleted");
    }
};
*/
